package com.cts.toyota.log.report;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.cts.toyota.log.bean.LogBean;
import com.cts.toyota.log.bean.Report;
import com.cts.toyota.log.bean.ReportBean;
import com.cts.toyota.log.constants.Constants;
import com.cts.toyota.log.exception.AccessLogException;
import com.cts.toyota.log.util.FileUtil;
import com.cts.toyota.log.util.ReportUtil;
import com.cts.toyota.log.util.StringUtil;
import com.cts.toyota.log.util.ohs.OHSUtil;
import com.cts.toyota.log.util.wl.WeblogicUtil;

/**
 * Class to generate the access log report.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class GenerateReport {
	
	int logFormatType = Constants.WEBLOGIC_LOG_FORMAT_TYPE;
	
	public GenerateReport(int logFormatType) {
		this.logFormatType = logFormatType;
	}

	/**
	 * @param baseDirectory - Base directory contains the access logs
	 * @param fileFilter - Name to Filter the files.
	 * @param startDateTime - Start Time from which the report has to be considered.
	 * @param endDateTime - End Time till which the report has to be considered.
	 * @param logFormat - Access log Format.
	 * @param percentile - Percentile for which the report has to be generated.
	 * @return Report - Report object contains the Report bean list and Access log list.
	 * @throws AccessLogException
	 */
	public Report getGeneratedReport(File baseDirectory, String fileFilter, String startDateTime, String endDateTime, String logFormat, int percentile) throws AccessLogException {
		Report report = new Report();
		try {
			
			List<LogBean> logBeanList = new ArrayList<>();
					
			// Get all the files within the directory and its sub directory
			List<File> fileList = FileUtil.getFileList(baseDirectory);
			
			// File the files based on the file name filter condition
			fileList = FileUtil.filterFiles(fileList, fileFilter);
			
			// Read the contents of all file and filter Weblogic/OHS related contents
			List<String> fileContent = FileUtil.getAllFilesContent(fileList);
			
			// Remove tab space and continuous white space with single space
			fileContent = StringUtil.alterLogToProcess(fileContent);
			
			// Convert each line into bean
			if(logFormatType == Constants.WEBLOGIC_LOG_FORMAT_TYPE){				
				logBeanList = WeblogicUtil.convertWeblogicAccessToLogBean(fileContent, startDateTime, endDateTime, logFormat);
			} else if (logFormatType == Constants.OHS_LOG_FORMAT_TYPE) {
				logBeanList = OHSUtil.convertOhsAccessToLogBean(fileContent, startDateTime, endDateTime, logFormat);
			}
			
			// Sort the collection based on Date (compareTo Method is overridden in Bean)
			Collections.sort(logBeanList);
			
			// Create Map with URL as the key
			Map<String, List<LogBean>> logBeanMap = ReportUtil.convertToUrlBasedMap(logBeanList);
			
			// Calculate Average and Percentile
			Map<String, ReportBean> reportBeanMap = new HashMap<>();
			for (Iterator<String> iterator = logBeanMap.keySet().iterator(); iterator.hasNext();) {
				String logBeanMapKey = iterator.next();
				ReportBean reportBean = ReportUtil.calculateAverageAndPercentile(logBeanMapKey, logBeanMap.get(logBeanMapKey), percentile);
				reportBeanMap.put(logBeanMapKey, reportBean);
			}
			report.setReportBeanMap(reportBeanMap);
			report.setLogBeanMap(logBeanMap);
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
		return report;
	}
}
