package com.cts.toyota.log.report.main;

import javax.swing.UIManager;

import org.apache.log4j.Logger;

import com.cts.toyota.log.constants.Constants;
import com.cts.toyota.log.ui.ReportMain;

/**
* Java Swings UI based application to analyze, prepare and export the report of server access log.
* Report will have URL, Average Response Time and the Percentile Response Time
*
* @author  274664 (Ravikumar J), 323596 (Sankar S)
* @version 1.0
* @since   2017-5-26 
*/
public class Main {

	private static final Logger LOGGER = Logger.getLogger(Main.class);

	/**
	 * Hiding with Empty Constructor
	 */
	private Main() {}
	
	/**
	 * Main Method of the project
	 * 
	 * @param args - Arguments to be passed to main method while loading.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(Constants.UI_LOOK_AND_FEEL);
			new ReportMain();
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
}
