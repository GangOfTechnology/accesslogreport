package com.cts.toyota.log.util.ohs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cts.toyota.log.bean.IndexBean;
import com.cts.toyota.log.bean.LogBean;
import com.cts.toyota.log.constants.Constants;
import com.cts.toyota.log.exception.AccessLogException;
import com.cts.toyota.log.util.ReportUtil;


/**
 * OHS Util to convert OHS server access log to bean.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class OHSUtil {

	/**
	 * Hiding with Empty Constructor
	 */
	private OHSUtil() {}

	/**
	 * Method to convert flat OHS server access file to Log Bean.
	 * 
	 * @param inputList - List containing the Access log lines.
	 * @param startDateTime - Start Time from which the report has to be considered.
	 * @param endDateTime - End Time till which the report has to be considered.
	 * @param ohsLogFormat - OHS Access log Format.
	 * @return List<LogBean> - List containing each Access log line as Log Bean.
	 * @throws AccessLogException
	 */
	public static List<LogBean> convertOhsAccessToLogBean(List<String> inputList, String startDateTime,
			String endDateTime, String ohsLogFormat) throws AccessLogException {
		List<LogBean> logBeanList;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
		IndexBean indexBean = new IndexBean();
		try {
			Date startTime = simpleDateFormat.parse(startDateTime);
			Date endTime = simpleDateFormat.parse(endDateTime);

			String[] logFormat = ohsLogFormat.split(" ");
			int formatLength = logFormat.length;
			for (int index = 0; index < logFormat.length; index++) {
				if (logFormat[index].equals(Constants.OHS_DATE)) {
					indexBean.setDateIndex(index);
				} else if (logFormat[index].equals(Constants.OHS_CS_URI)) {
					indexBean.setUrlIndex(index);
				} else if (logFormat[index].equals(Constants.OHS_TIME_TAKEN)) {
					indexBean.setTimeTakenIndex(index);
				} 
			}
			logBeanList = extractLogBeanList(inputList, formatLength, indexBean, startTime, endTime);
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
		return logBeanList;
	}

	/**
	 * Method to convert Access log String List to Log Bean List.
	 * 
	 * @param inputList - List containing the Access log lines.
	 * @param formatLength - OHS Access log Format word length.
	 * @param indexBean - Bean containing the indexes for each word format in log
	 * @param startTime - Start Time from which the report has to be considered.
	 * @param endTime - End Time till which the report has to be considered.
	 * @return List<LogBean> - List containing each Access log line as Log Bean.
	 */
	private static List<LogBean> extractLogBeanList(List<String> inputList, int formatLength, IndexBean indexBean, Date startTime, Date endTime) {
		List<LogBean> logBeanList = new ArrayList<>();
		String line;
		String url;
		for (Iterator<String> iterator = inputList.iterator(); iterator.hasNext();) {
			line = iterator.next();
			String dtline = line.substring(line.indexOf('['),line.indexOf(']')+1);
			dtline = dtline.substring(1, dtline.indexOf(' '));
			line = line.substring(0,line.indexOf('['))+dtline+line.substring(line.indexOf(']')+1,line.length());
			String[] lineSplit = line.split(" ");
			if (lineSplit.length != formatLength) {
				continue;
			}
			Date accessTime = ReportUtil.parseDate(Constants.OHS_DATE_TIME_FORMAT, lineSplit[indexBean.getDateIndex()]);
			if (accessTime != null && ReportUtil.isDateWithinLimit(accessTime, startTime, endTime)) {
				LogBean logBean = new LogBean();
				url = lineSplit[indexBean.getUrlIndex()];
				if(url.indexOf('?')>=0){					
					url = url.substring(0, url.indexOf('?'));
				} 
				if(url.indexOf(';')>=0){					
					url = url.substring(0, url.indexOf(';'));
				} 
				logBean.setUrl(url);
				logBean.setTimeTaken(Double.valueOf(lineSplit[indexBean.getTimeTakenIndex()])/1000000);
				logBean.setDate(accessTime);
				logBeanList.add(logBean);
			}
		}
		return logBeanList;
	}
}
