package com.cts.toyota.log.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cts.toyota.log.bean.LogBean;
import com.cts.toyota.log.bean.ReportBean;
import com.cts.toyota.log.exception.AccessLogException;

/**
 * Report Util to convert server access log to bean.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class ReportUtil {

	/**
	 * Logger to log the exceptions
	 */
	private static final Logger LOGGER = Logger.getLogger(ReportUtil.class);
	
	/**
	 * Hiding with Empty Constructor
	 */
	private ReportUtil() {}
	
	/**
	 * Method to check whether the encountered date is within the date for which the report is requested to extract.
	 * 
	 * @param accessTime - Date/Time of Access log line.
	 * @param startTime - Start Time from which the report has to be considered.
	 * @param endTime - End Time till which the report has to be considered.
	 * @return boolean - Returns true if date is within limit, else false.
	 */
	public static boolean isDateWithinLimit(Date accessTime, Date startTime, Date endTime) {
		return (accessTime.equals(startTime) || accessTime.equals(endTime)
				|| (accessTime.after(startTime) && accessTime.before(endTime)));
	}
	
	/**
	 * Method to convert the string value to date object
	 * 
	 * @param dateFormat - Format of the date in access log.
	 * @param date - String value to be parsed and convert as Java Util Date.
	 * @return Date - Return parsed Date.
	 */
	public static Date parseDate(String dateFormat, String date) {
		Date parsedDate = null;
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
			parsedDate = simpleDateFormat.parse(date);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return parsedDate;
	}
	
	/**
	 * Method to convert Log bean to Access log URL based Map
	 * 
	 * @param logBeanList - List containing each Access log line as Log Bean.
	 * @return Map<String, List<LogBean>> - URL based Map
	 * @throws AccessLogException
	 */
	public static Map<String, List<LogBean>> convertToUrlBasedMap(List<LogBean> logBeanList)
			throws AccessLogException {
		Map<String, List<LogBean>> urlBasedMap = new HashMap<>();
		try {
			for (Iterator<LogBean> iterator = logBeanList.iterator(); iterator.hasNext();) {
				LogBean logBean = iterator.next();
				if (urlBasedMap.containsKey(logBean.getUrl())) {
					urlBasedMap.get(logBean.getUrl()).add(logBean);
				} else {
					List<LogBean> logBeanTempList = new ArrayList<>();
					logBeanTempList.add(logBean);
					urlBasedMap.put(logBean.getUrl(), logBeanTempList);
				}
			}
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
		return urlBasedMap;
	}

	/**
	 * Method to calculate the average and percentile report for each url
	 * 
	 * @param logBeanList - List containing each Access log line as Log Bean.
	 * @param percentile - Percentile for which the report has to be generated.
	 * @return ReportBean - Bean containing Average and Percentile Report.
	 * @throws AccessLogException
	 */
	public static ReportBean calculateAverageAndPercentile(String url, List<LogBean> logBeanList, int percentile)
			throws AccessLogException {
		ReportBean reportBean = new ReportBean();
		try {
			List<Double> timeTakenList = new ArrayList<>();
			for (Iterator<LogBean> iterator = logBeanList.iterator(); iterator.hasNext();) {
				LogBean logBean = iterator.next();
				timeTakenList.add(logBean.getTimeTaken());
			}
			double sumOfTimeTaken = 0.0;
			Collections.sort(timeTakenList);
			for (Iterator<Double> iterator = timeTakenList.iterator(); iterator.hasNext();) {
				sumOfTimeTaken += iterator.next();
			}

			int percentileIndex = (timeTakenList.size() * percentile) / 100;
			reportBean.setUrl(url);
			reportBean.setNoOfHits(logBeanList.size());
			reportBean.setPercentile(Integer.toString(percentile));
			reportBean.setPercentileValue(timeTakenList.get(percentileIndex));
			reportBean.setAverage(sumOfTimeTaken / timeTakenList.size());
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
		return reportBean;
	}
	
	/**
	 * Method to sort the list based on Average and Percentile.
	 * 
	 * @param reportBeanMap - Map contains Report Bean which is having average, percentile for each urls.
	 * @return Map<String, List<ReportBean>> - Sorted Map for Average and Percentile.
	 */
	public static Map<String, List<ReportBean>> getSortedUrlList(Map<String, ReportBean> reportBeanMap){
		Map<String, List<ReportBean>> sortedUrlList = new HashMap<>();
		List<ReportBean> averageSortedList = new ArrayList<>();
		List<ReportBean> percentileSortedList = new ArrayList<>();
		for (Iterator<String> iterator = reportBeanMap.keySet().iterator(); iterator.hasNext();) {
			String url = iterator.next();
			ReportBean reportBean = reportBeanMap.get(url);
			averageSortedList.add(reportBean);
			percentileSortedList.add(reportBean.getAvgExchPercBean(reportBean));
		}
		Collections.sort(averageSortedList);
		Collections.sort(percentileSortedList);
		sortedUrlList.put("Average",averageSortedList);
		sortedUrlList.put("Percentile",percentileSortedList);
		return sortedUrlList;
	}
}
