package com.cts.toyota.log.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cts.toyota.log.exception.AccessLogException;

/**
 * String Util to alter Access logs.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class StringUtil {
	
	/**
	 * Hiding with Empty Constructor
	 */
	private StringUtil() {}

	/**
	 * @param inputList - List containing the Access log lines.
	 * @return List<String> - List containing the altered access log lines.
	 * @throws AccessLogException
	 */
	public static List<String> alterLogToProcess(List<String> inputList) throws AccessLogException {
		List<String> altertedList = new ArrayList<>();
		String line = "";
		try {
			for (Iterator<String> iterator = inputList.iterator(); iterator.hasNext();) {
				line = iterator.next();
				if (line != null) {
					line = line.replaceAll("\t", " ");
					line = line.replaceAll("\\s+", " ");
					altertedList.add(line);
				}
			}
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
		return altertedList;
	}
}
