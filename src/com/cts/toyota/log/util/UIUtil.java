package com.cts.toyota.log.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jfree.chart.LegendItem;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import com.cts.toyota.log.bean.LogBean;
import com.cts.toyota.log.bean.Report;
import com.cts.toyota.log.bean.ReportBean;
import com.cts.toyota.log.excel.ExportToExcel;
import com.cts.toyota.log.exception.AccessLogException;

/**
 * UI Util to support the creation of Swings UI.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class UIUtil {
	
	/**
	 * Hiding with Empty Constructor
	 */
	private UIUtil() {}
	
	/**
	 * This method is used to add the component in container based on the location specified.
	 * 
	 * @param container - Container in which the Component has to be added.
	 * @param component - Component to be added in the Container.
	 * @param x - X Axis Position.
	 * @param y - Y Axis Position.
	 * @param w - Width of the component.
	 * @param h - Height of the component.
	 * @return Nothing.
	 */
	public static void addComponent(Container container, Component component, int x, int y, int w, int h) {
		component.setBounds(x, y, w, h);
		container.add(component);
	}

	/**
	 * Method to show the report panel.
	 * 
	 * @param frame - Parent Frame.
	 * @param contentPane - Panel which has to be shown in the parent frame.
	 */
	public static void openReportPanel(JFrame frame, JPanel contentPane){
		frame.setContentPane(contentPane);
		frame.revalidate();
		frame.repaint();
	}
	
	/**
	 * This method is used to select the Access log files base directory.
	 * 
	 * @return Nothing.
	 */
	public static void selectLogBaseDirectory(JTextField logFilesDirectoryField) {
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("Select Log Base Directory");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			File logBaseDirectory = chooser.getSelectedFile();
			logFilesDirectoryField.setText(logBaseDirectory.getAbsolutePath().trim());
		}
	}
	
	/**
	 * This method is used to export the report.
	 * 
	 * @return Nothing.
	 */
	public static void exportReport(JFrame frame, Report report, JComboBox<Integer> percentileCombo) throws AccessLogException {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Path to Save File");
		int userSelection = fileChooser.showSaveDialog(frame);
		if (userSelection == JFileChooser.APPROVE_OPTION) {
			File fileToSave = fileChooser.getSelectedFile();
			ExportToExcel exportToExcel = new ExportToExcel();
			Map<String, ReportBean> reportBeanMap = report.getReportBeanMap();
			exportToExcel.exportDateToExcel(reportBeanMap, fileToSave.getAbsolutePath(),
					(Integer) percentileCombo.getSelectedItem());
		}
		JOptionPane.showMessageDialog(frame, "Report Exported Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * This method is used to set the Report Start Time.
	 * 
	 * @return Nothing.
	 */
	public static String setReportTime(JComboBox<Integer> yearCombo, JComboBox<Integer> monthCombo, JComboBox<Integer> dayCombo, 
			JComboBox<Integer> hourCombo, JComboBox<Integer> minutesCombo, JComboBox<Integer> secondsCombo) {
		String reportTime = yearCombo.getSelectedItem() + "";
		if (monthCombo.getSelectedItem().toString().trim().length() == 1) {
			reportTime += "-0" + monthCombo.getSelectedItem();
		} else {
			reportTime += "-" + monthCombo.getSelectedItem();
		}

		if (dayCombo.getSelectedItem().toString().trim().length() == 1) {
			reportTime += "-0" + dayCombo.getSelectedItem();
		} else {
			reportTime += "-" + dayCombo.getSelectedItem();
		}

		if (hourCombo.getSelectedItem().toString().trim().length() == 1) {
			reportTime += " 0" + hourCombo.getSelectedItem();
		} else {
			reportTime += " " + hourCombo.getSelectedItem();
		}

		if (minutesCombo.getSelectedItem().toString().trim().length() == 1) {
			reportTime += ":0" + minutesCombo.getSelectedItem();
		} else {
			reportTime += ":" + minutesCombo.getSelectedItem();
		}

		if (secondsCombo.getSelectedItem().toString().trim().length() == 1) {
			reportTime += ":0" + secondsCombo.getSelectedItem();
		} else {
			reportTime += ":" + secondsCombo.getSelectedItem();
		}
		return reportTime;
	}
	
	/**
	 * Method to create the dataset for graph.
	 * 
	 * @param name - Series/Url name.
	 * @param logBeanList - List containing each Access log line as Log Bean.
	 * @return TimeSeriesCollection - TimeSeriesCollection for the list passed.
	 */
	public static TimeSeriesCollection createDataset(final String name, List<LogBean> logBeanList) {
		TimeSeries series = new TimeSeries(name);
		Map<String, Double> dateValueMap = new HashMap<>();
		boolean valueUpdated;
		for (Iterator<LogBean> iterator = logBeanList.iterator(); iterator.hasNext();) {
			LogBean logBean = iterator.next();
			Date reportDate = logBean.getDate();
			RegularTimePeriod t = new Second(reportDate);
			valueUpdated = false;
			if (dateValueMap.get(reportDate) == null || dateValueMap.get(reportDate) < logBean.getTimeTaken()) {
				dateValueMap.put(reportDate.toString(), logBean.getTimeTaken());
				valueUpdated = true;
			}
			if (valueUpdated) {
				series.addOrUpdate(t, logBean.getTimeTaken());
			} else {
				series.add(t, logBean.getTimeTaken());
			}
		}
		return new TimeSeriesCollection(series);
	}
	
	/**
	 * Method to create legend panel for the graph with scroll.
	 * 
	 * @param plot - Plot contains the graph.
	 * @param urlMultiSelectList - Multi select list contains the urls.
	 * @param urlColorMap - Map contains the color for all the urls.
	 * @return JPanel - Panel contains the legend.
	 */
	@SuppressWarnings("unchecked")
	public static JPanel createLegendPanel(XYPlot plot, JList<String> urlMultiSelectList, Map<String, Color> urlColorMap) {
		JPanel panel = new JPanel(new GridLayout(0, 1, 5, 5));
		for (Iterator<LegendItem> iterator = plot.getLegendItems().iterator(); iterator.hasNext();) {
			LegendItem item = iterator.next();
			JLabel label = new JLabel(item.getLabel());
			label.setFont(item.getLabelFont());
			String rgbColor = item.getFillPaint().toString();
			String redColorValue = rgbColor.substring(rgbColor.indexOf("r=") + 2, rgbColor.indexOf("g=") - 1);
			String greenColorValue = rgbColor.substring(rgbColor.indexOf("g=") + 2, rgbColor.indexOf("b=") - 1);
			String blueColorValue = rgbColor.substring(rgbColor.indexOf("b=") + 2, rgbColor.indexOf(']'));
			Color color = new Color(Integer.valueOf(redColorValue), Integer.valueOf(greenColorValue), Integer.valueOf(blueColorValue));
			label.setForeground(color);
			panel.add(label);
			JLabel listLabel = new JLabel(item.getLabel());
			DefaultListModel<String> listModel = (DefaultListModel<String>) urlMultiSelectList.getModel();
			listModel.addElement(listLabel.getText());
			urlColorMap.put(listLabel.getText(), color);
		}
		return panel;
	}
}
