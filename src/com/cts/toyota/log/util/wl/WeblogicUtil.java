package com.cts.toyota.log.util.wl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cts.toyota.log.bean.IndexBean;
import com.cts.toyota.log.bean.LogBean;
import com.cts.toyota.log.constants.Constants;
import com.cts.toyota.log.exception.AccessLogException;
import com.cts.toyota.log.util.ReportUtil;


/**
 * Weblogic Util to convert weblogic server access log to bean.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class WeblogicUtil {

	/**
	 * Hiding with Empty Constructor
	 */
	private WeblogicUtil() {}

	/**
	 * Method to convert flat weblogic server access file to Weblogic Bean.
	 * 
	 * @param inputList - List containing the Access log lines.
	 * @param startDateTime - Start Time from which the report has to be considered.
	 * @param endDateTime - End Time till which the report has to be considered.
	 * @param accessLogFormat - Weblogic Access log Format.
	 * @return List<WeblogicLogBean> - List containing each Access log line as Weblogic Bean.
	 * @throws AccessLogException
	 */
	public static List<LogBean> convertWeblogicAccessToLogBean(List<String> inputList, String startDateTime,
			String endDateTime, String accessLogFormat) throws AccessLogException {
		List<LogBean> logBeanList;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
		IndexBean indexBean = new IndexBean();
		try {
			Date startTime = simpleDateFormat.parse(startDateTime);
			Date endTime = simpleDateFormat.parse(endDateTime);

			String[] logFormat = accessLogFormat.split(" ");
			int formatLength = logFormat.length;
			for (int index = 0; index < logFormat.length; index++) {
				if (logFormat[index].equals(Constants.WEBLOGIC_DATE)) {
					indexBean.setDateIndex(index);
				} else if (logFormat[index].equals(Constants.WEBLOGIC_TIME)) {
					indexBean.setTimeIndex(index);
				} else if (logFormat[index].equals(Constants.WEBLOGIC_CS_URI_STEM) || logFormat[index].equals(Constants.WEBLOGIC_CS_URI)) {
					indexBean.setUrlIndex(index);
				} else if (logFormat[index].equals(Constants.WEBLOGIC_TIME_TAKEN)) {
					indexBean.setTimeTakenIndex(index);
				} 
			}
			logBeanList = extractWeblogicBeanList(inputList, formatLength, indexBean, startTime, endTime);
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
		return logBeanList;
	}

	/**
	 * Method to convert Access log String List to Weblogic Bean List.
	 * 
	 * @param inputList - List containing the Access log lines.
	 * @param formatLength - Weblogic Access log Format word length.
	 * @param indexBean - Bean containing the indexes for each word format in log
	 * @param startTime - Start Time from which the report has to be considered.
	 * @param endTime - End Time till which the report has to be considered.
	 * @return List<WeblogicLogBean> - List containing each Access log line as Weblogic Bean.
	 */
	private static List<LogBean> extractWeblogicBeanList(List<String> inputList, int formatLength, IndexBean indexBean, Date startTime, Date endTime) {
		List<LogBean> logBeanList = new ArrayList<>();
		String line;
		String url;
		for (Iterator<String> iterator = inputList.iterator(); iterator.hasNext();) {
			line = iterator.next();
			String[] lineSplit = line.split(" ");
			if (lineSplit.length != formatLength) {
				continue;
			}
			Date accessTime = ReportUtil.parseDate(Constants.WEBLOGIC_DATE_TIME_FORMAT, lineSplit[indexBean.getDateIndex()] + " " + lineSplit[indexBean.getTimeIndex()]);
			if (accessTime != null && ReportUtil.isDateWithinLimit(accessTime, startTime, endTime)) {
				LogBean logBean = new LogBean();
				url = lineSplit[indexBean.getUrlIndex()];
				if(url.indexOf('?')>=0){					
					url = url.substring(0, url.indexOf('?'));
				} 
				if(url.indexOf(';')>=0){					
					url = url.substring(0, url.indexOf(';'));
				} 
				logBean.setUrl(url);
				logBean.setTimeTaken(Double.valueOf(lineSplit[indexBean.getTimeTakenIndex()]));
				logBean.setDate(accessTime);
				logBeanList.add(logBean);
			}
		}
		return logBeanList;
	}
}
