package com.cts.toyota.log.util;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Custom class to render Swing table.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class ColumnRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 6462505593702968340L;

	Color backgroundColor;
	
	Color foregroundColor;

	/**
	 * Constructor to set the background, foreground and alignment of the cell.
	 * 
	 * @param bkgnd - Background color of the cell.
	 * @param foregnd - Foreground color of the cell.
	 * @param alignment - Alignment of the cell.
	 */
	public ColumnRenderer(Color bkgnd, Color foregnd, int alignment) {
		super();
		backgroundColor = bkgnd;
		foregroundColor = foregnd;
		super.setHorizontalAlignment(alignment);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		cell.setBackground(backgroundColor);
		cell.setForeground(foregroundColor);
		return cell;
	}
}