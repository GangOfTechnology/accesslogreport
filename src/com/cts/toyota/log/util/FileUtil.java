package com.cts.toyota.log.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cts.toyota.log.constants.Constants;
import com.cts.toyota.log.exception.AccessLogException;

/**
 * File Util to Manipulate Access logs.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class FileUtil {

	/**
	 * Hiding with Empty Constructor
	 */
	private FileUtil() {}
	
	/**
	 * Method to extract all files within the base directory and its sub directory.
	 * 
	 * @param baseDirectory - Base directory containing access logs of all server instances.
	 * @return List<File> - List containing all files within the base directory
	 * @throws AccessLogException
	 */
	public static List<File> getFileList(File baseDirectory) throws AccessLogException {
		List<File> finalFileList = new ArrayList<>();
		try {
			List<File> fileList = new ArrayList<>();
			fileList.add(baseDirectory);
			File file;
			while (!fileList.isEmpty()) {
				file = fileList.get(0);
				fileList.remove(0);
				if(file.isFile()){
					finalFileList.add(file);
					continue;
				}
				File[] listFiles = file.listFiles();
				for (int index = 0; index < listFiles.length; index++) {
					fileList.add(listFiles[index]);
				}
			}
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
		return finalFileList;
	}

	/**
	 * Method to filter the files based on the filter.
	 * 
	 * @param fileList - List containing all files within the base directory.
	 * @param fileNameFilter - Wildcard File name filter.
	 * @return List<File> - List contains the files filtered based on the applied filter.
	 * @throws AccessLogException
	 */
	public static List<File> filterFiles(List<File> fileList, String fileNameFilter) throws AccessLogException {
		List<File> filteredFileList = new ArrayList<>();
		try {
			String filter = fileNameFilter;
			boolean preFilter = true;
			if ("*".equals(fileNameFilter.trim())) {
				return fileList;
			} else if (fileNameFilter.trim().startsWith("*")) {
				filter = fileNameFilter.substring(1, fileNameFilter.length());
			} else if (fileNameFilter.trim().endsWith("*")) {
				filter = fileNameFilter.substring(0, fileNameFilter.indexOf('*'));
				preFilter = false;
			}
			for (Iterator<File> iterator = fileList.iterator(); iterator.hasNext();) {
				File file = iterator.next();
				if ((preFilter && file.getName().endsWith(filter)) || (!preFilter && file.getName().startsWith(filter))) {
					filteredFileList.add(file);
				} 
			}
		} catch (Exception e) {
			throw e;
		}
		return filteredFileList;
	}

	/**
	 * Method to extract all lines in log files as a list.
	 * 
	 * @param fileList - List contains the filtered files.
	 * @return List<String> - List contains all lines of access logs.
	 * @throws AccessLogException
	 */
	public static List<String> getAllFilesContent(List<File> fileList) throws AccessLogException {
		List<String> fileContent = new ArrayList<>();
		try {
			for (Iterator<File> iterator = fileList.iterator(); iterator.hasNext();) {
				File file = iterator.next();
				fileContent.addAll(getFileContent(file));
			}
		} catch (Exception e) {
			throw e;
		}
		return fileContent;
	}

	/**
	 * Method to read the file content and return it as a list.
	 * 
	 * @param file - File for which the content to read.
	 * @return List<String> - List contains the Access log content.
	 * @throws AccessLogException
	 */
	private static List<String> getFileContent(File file) throws AccessLogException {
		List<String> fileContent = new ArrayList<>();
		BufferedReader bufferedReader = null;
		try (FileReader fileReader = new FileReader(file)) {
			bufferedReader = new BufferedReader(fileReader);
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				if (line.startsWith(Constants.WEBLOGIC_CONTENT_STARTS_WITH)) {
					continue;
				}
				fileContent.add(line);
			}
		} catch (Exception e) {
			throw new AccessLogException(e);
		} finally {
			closeReader(bufferedReader);
		}
		return fileContent;
	}
	
	/**
	 * Method to close the Buffered Reader handle to the file.
	 * 
	 * @param bufferedReader - Buffered Reader instance need to be closed
	 * @throws AccessLogException
	 */
	private static void closeReader(BufferedReader bufferedReader) throws AccessLogException {
		try {
			if (bufferedReader != null){
				bufferedReader.close();
			}
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
	}
}
