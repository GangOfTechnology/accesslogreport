package com.cts.toyota.log.constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Class contains the constant variables and its values.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class Constants {

	public static final String UI_LOOK_AND_FEEL = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
	
	public static final String WEBLOGIC_CONTENT_STARTS_WITH = "#";
	
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm:ss";
	
	public static final String WEBLOGIC_DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm:ss";

	public static final String WEBLOGIC_DATE = "date";

	public static final String WEBLOGIC_TIME = "time";

	public static final String WEBLOGIC_TIME_TAKEN = "time-taken";
	
	public static final String WEBLOGIC_CS_URI = "cs-uri";

	public static final String WEBLOGIC_CS_URI_STEM = "cs-uri-stem";
	
	public static final String OHS_DATE_TIME_FORMAT = "dd/MMM/yyyy:hh:mm:ss";
	
	public static final String OHS_DATE = "%t";

	public static final String OHS_TIME_TAKEN = "%D";
	
	public static final String OHS_CS_URI = "%U";

	public static final String OHS_DEFAULT_LOG_FORMAT = "%h %A %u %t %m %U %H %>s %b %P %D";
	
	public static final int WEBLOGIC_LOG_FORMAT_TYPE = 0;
	
	public static final int OHS_LOG_FORMAT_TYPE = 1;

	public static final String CLEAR = "Clear";

	public static final String ERROR = "Error";
	
	public static final String FILTER = "Filter";
	
	public static final String GRAPH = "Graph";
	
	public static final String PERCENTILE = "Percentile";

	public static final String START_YEAR_COMBO = "StartYearCombo";

	public static final String START_MONTH_COMBO = "StartMonthCombo";

	public static final String START_DAY_COMBO = "StartDayCombo";

	public static final String START_HOUR_COMBO = "StartHourCombo";

	public static final String START_MINUTE_COMBO = "StartMinuteCombo";

	public static final String START_SECOND_COMBO = "StartSecondCombo";

	protected static final List<String> START_DATE_TIME_LIST = new ArrayList<>();
	static {
		START_DATE_TIME_LIST.add(START_YEAR_COMBO);
		START_DATE_TIME_LIST.add(START_MONTH_COMBO);
		START_DATE_TIME_LIST.add(START_DAY_COMBO);
		START_DATE_TIME_LIST.add(START_HOUR_COMBO);
		START_DATE_TIME_LIST.add(START_MINUTE_COMBO);
		START_DATE_TIME_LIST.add(START_SECOND_COMBO);

	}
	
	public static final String END_YEAR_COMBO = "EndYearCombo";

	public static final String END_MONTH_COMBO = "EndMonthCombo";

	public static final String END_DAY_COMBO = "EndDayCombo";

	public static final String END_HOUR_COMBO = "EndHourCombo";

	public static final String END_MINUTE_COMBO = "EndMinuteCombo";

	public static final String END_SECOND_COMBO = "EndSecondCombo";

	protected static final List<String> END_DATE_TIME_LIST = new ArrayList<>();
	static {
		END_DATE_TIME_LIST.add(END_YEAR_COMBO);
		END_DATE_TIME_LIST.add(END_MONTH_COMBO);
		END_DATE_TIME_LIST.add(END_DAY_COMBO);
		END_DATE_TIME_LIST.add(END_HOUR_COMBO);
		END_DATE_TIME_LIST.add(END_MINUTE_COMBO);
		END_DATE_TIME_LIST.add(END_SECOND_COMBO);
	}

	/**
	 * Hiding with Empty Constructor
	 */
	private Constants() {
	}
	
	/**
	 * @return List<String> - List contains the Start Date Time Constants.
	 */
	public static List<String> getStartDateTimeList(){
		return START_DATE_TIME_LIST;
	}
	
	/**
	 * @return List<String> - List contains the End Date Time Constants.
	 */
	public static List<String> getEndDateTimeList(){
		return END_DATE_TIME_LIST;
	}

}
