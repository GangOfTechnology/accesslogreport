package com.cts.toyota.log.exception;

/**
 * Custom Exception to handle exceptions.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class AccessLogException extends Exception {

	private static final long serialVersionUID = 3694964607158755018L;
	
	/**
	 * Constructor for custom exception
	 * 
	 * @param e - Exception thrown
	 */
	public AccessLogException(Exception e) {
		super(e);
	}
}
