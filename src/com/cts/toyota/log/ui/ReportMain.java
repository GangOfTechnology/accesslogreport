package com.cts.toyota.log.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.time.TimeSeriesCollection;

import com.cts.toyota.log.bean.LogBean;
import com.cts.toyota.log.bean.Report;
import com.cts.toyota.log.bean.ReportBean;
import com.cts.toyota.log.constants.Constants;
import com.cts.toyota.log.exception.AccessLogException;
import com.cts.toyota.log.report.GenerateReport;
import com.cts.toyota.log.util.ColumnRenderer;
import com.cts.toyota.log.util.ReportUtil;
import com.cts.toyota.log.util.UIUtil;

/**
* Java Swings UI based application to analyze, prepare and export the report of server access log.
* Report will have URL, Average Response Time and the Percentile Response Time
*
* @author  274664 (Ravikumar J), 323596 (Sankar S)
* @version 1.0
* @since   2017-5-26 
*/
public class ReportMain extends LogFrame implements ActionListener {
	
	private static final Logger LOGGER = Logger.getLogger(ReportMain.class);

	private static final long serialVersionUID = 1487241523961546933L;

	/**
	 * Java Swings UI to analyze and export server access log
	 */
	public ReportMain() {
		setTitle("Access Log Report");
		contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(null);

		logFormatPanel.setBorder(BorderFactory.createEtchedBorder());
		logFormatPanel.setLayout(null);
		logFormatPanel.setBackground(Color.LIGHT_GRAY);
		
		UIUtil.addComponent(logFormatPanel, logTypeLabel, 10, 15, 100, 30);
		
		weblogicRadioButton.setActionCommand("WeblogicRadio");
		weblogicRadioButton.setSelected(false);
		weblogicRadioButton.addActionListener(this);
		ohsRadioButton.setActionCommand("OHSRadio");
		ohsRadioButton.setSelected(false);
		ohsRadioButton.addActionListener(this);
		ButtonGroup logTypeButtonGroup = new ButtonGroup();
		logTypeButtonGroup.add(weblogicRadioButton);
		logTypeButtonGroup.add(ohsRadioButton);
		logTypeButtonGroup.clearSelection();

		UIUtil.addComponent(logFormatPanel, weblogicRadioButton, 130, 15, 90, 30);
		UIUtil.addComponent(logFormatPanel, ohsRadioButton, 223, 15, 100, 30);
		
		UIUtil.addComponent(logFormatPanel, logFormatLabel, 10, 60, 100, 30);

		logFormatField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				logFormat = logFormatField.getText().trim();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				logFormat = logFormatField.getText().trim();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				logFormat = logFormatField.getText().trim();
			}
		});
		UIUtil.addComponent(logFormatPanel, logFormatField, 130, 60, 500, 30);

		JButton clearButton = new JButton(Constants.CLEAR);
		clearButton.setActionCommand(Constants.CLEAR);
		clearButton.addActionListener(this);
		UIUtil.addComponent(logFormatPanel, clearButton, 650, 60, 100, 30);

		startTimePanel.setLayout(null);
		startTimePanel.setBorder(BorderFactory.createLoweredBevelBorder());
		startTimePanel.setBackground(Color.GRAY);

		UIUtil.addComponent(startTimePanel, startTimeLabel, 10, 40, 120, 25);

		for (int index = year; index > year - 10; index--) {
			startYearCombo.addItem(index);
		}
		startYearCombo.setActionCommand("StartYearCombo");
		startYearCombo.addActionListener(this);
		UIUtil.addComponent(startTimePanel, startYearLabel, 145, 10, 90, 25);
		UIUtil.addComponent(startTimePanel, startYearCombo, 130, 40, 70, 25);

		for (int index = 1; index <= 12; index++) {
			startMonthCombo.addItem(index);
		}
		startMonthCombo.setActionCommand("StartMonthCombo");
		startMonthCombo.addActionListener(this);
		UIUtil.addComponent(startTimePanel, startMonthLabel, 220, 10, 90, 25);
		UIUtil.addComponent(startTimePanel, startMonthCombo, 205, 40, 70, 25);

		for (int index = 1; index <= 31; index++) {
			startDayCombo.addItem(index);
		}
		startDayCombo.setActionCommand("StartDayCombo");
		startDayCombo.addActionListener(this);
		UIUtil.addComponent(startTimePanel, startDayLabel, 300, 10, 90, 25);
		UIUtil.addComponent(startTimePanel, startDayCombo, 280, 40, 70, 25);

		for (int index = 0; index <= 23; index++) {
			startHourCombo.addItem(index);
		}
		startHourCombo.setActionCommand("StartHourCombo");
		startHourCombo.addActionListener(this);
		UIUtil.addComponent(startTimePanel, startHourLabel, 380, 10, 90, 25);
		UIUtil.addComponent(startTimePanel, startHourCombo, 360, 40, 70, 25);

		for (int index = 0; index <= 59; index++) {
			startMinutesCombo.addItem(index);
		}
		startMinutesCombo.setActionCommand("StartMinuteCombo");
		startMinutesCombo.addActionListener(this);
		UIUtil.addComponent(startTimePanel, startMinuteLabel, 445, 10, 90, 25);
		UIUtil.addComponent(startTimePanel, startMinutesCombo, 435, 40, 70, 25);

		for (int index = 0; index <= 59; index++) {
			startSecondsCombo.addItem(index);
		}
		startSecondsCombo.setActionCommand("StartSecondCombo");
		startSecondsCombo.addActionListener(this);
		UIUtil.addComponent(startTimePanel, startSecondLabel, 520, 10, 90, 25);
		UIUtil.addComponent(startTimePanel, startSecondsCombo, 510, 40, 70, 25);
		UIUtil.addComponent(startTimePanel, startTimeValueLabel, 610, 40, 200, 25);

		endTimePanel.setLayout(null);
		endTimePanel.setBorder(BorderFactory.createEtchedBorder());
		endTimePanel.setBackground(Color.LIGHT_GRAY);

		UIUtil.addComponent(endTimePanel, endTimeLabel, 10, 40, 120, 25);

		for (int index = year; index > year - 10; index--) {
			endYearCombo.addItem(index);
		}
		endYearCombo.setActionCommand("EndYearCombo");
		endYearCombo.addActionListener(this);
		UIUtil.addComponent(endTimePanel, endYearLabel, 145, 10, 90, 25);
		UIUtil.addComponent(endTimePanel, endYearCombo, 130, 40, 70, 25);

		for (int index = 1; index <= 12; index++) {
			endMonthCombo.addItem(index);
		}
		endMonthCombo.setActionCommand("EndMonthCombo");
		endMonthCombo.addActionListener(this);
		UIUtil.addComponent(endTimePanel, endMonthLabel, 220, 10, 90, 25);
		UIUtil.addComponent(endTimePanel, endMonthCombo, 205, 40, 70, 25);

		for (int index = 1; index <= 31; index++) {
			endDayCombo.addItem(index);
		}
		endDayCombo.setActionCommand("EndDayCombo");
		endDayCombo.addActionListener(this);
		UIUtil.addComponent(endTimePanel, endDayLabel, 300, 10, 90, 25);
		UIUtil.addComponent(endTimePanel, endDayCombo, 280, 40, 70, 25);

		for (int index = 0; index <= 23; index++) {
			endHourCombo.addItem(index);
		}
		endHourCombo.setActionCommand("EndHourCombo");
		endHourCombo.addActionListener(this);
		UIUtil.addComponent(endTimePanel, endHourLabel, 380, 10, 90, 25);
		UIUtil.addComponent(endTimePanel, endHourCombo, 360, 40, 70, 25);

		for (int index = 0; index <= 59; index++) {
			endMinutesCombo.addItem(index);
		}
		endMinutesCombo.setActionCommand("EndMinuteCombo");
		endMinutesCombo.addActionListener(this);
		UIUtil.addComponent(endTimePanel, endMinuteLabel, 445, 10, 90, 25);
		UIUtil.addComponent(endTimePanel, endMinutesCombo, 435, 40, 70, 25);

		for (int index = 0; index <= 59; index++) {
			endSecondsCombo.addItem(index);
		}
		endSecondsCombo.setActionCommand("EndSecondCombo");
		endSecondsCombo.addActionListener(this);
		UIUtil.addComponent(endTimePanel, endSecondLabel, 520, 10, 90, 25);
		UIUtil.addComponent(endTimePanel, endSecondsCombo, 510, 40, 70, 25);
		UIUtil.addComponent(endTimePanel, endTimeValueLabel, 610, 40, 200, 25);

		logFilesSelectPanel.setLayout(null);
		logFilesSelectPanel.setBorder(BorderFactory.createLoweredBevelBorder());
		logFilesSelectPanel.setBackground(Color.GRAY);

		JLabel logFilesSelectLabel = new JLabel("Logfiles Directory :: ");
		UIUtil.addComponent(logFilesSelectPanel, logFilesSelectLabel, 10, 20, 120, 30);

		logFilesDirectoryField.setEditable(false);
		UIUtil.addComponent(logFilesSelectPanel, logFilesDirectoryField, 130, 20, 500, 30);

		JButton chooseLogFileDirectoryButton = new JButton("Select");
		chooseLogFileDirectoryButton.setActionCommand("SelectBaseLogDirectory");
		chooseLogFileDirectoryButton.addActionListener(this);
		UIUtil.addComponent(logFilesSelectPanel, chooseLogFileDirectoryButton, 650, 20, 100, 30);

		percentilePanel.setLayout(null);
		percentilePanel.setBorder(BorderFactory.createEtchedBorder());
		percentilePanel.setBackground(Color.LIGHT_GRAY);

		JLabel percentileLabel = new JLabel("Percentile :: ");
		UIUtil.addComponent(percentilePanel, percentileLabel, 10, 20, 120, 25);

		for (int index = 1; index <= 100; index++) {
			percentileCombo.addItem(index);
		}
		UIUtil.addComponent(percentilePanel, percentileCombo, 130, 20, 120, 25);

		filterPanel.setLayout(null);
		filterPanel.setBorder(BorderFactory.createLoweredBevelBorder());
		filterPanel.setBackground(Color.GRAY);

		JLabel filterLabel = new JLabel("File Filter :: ");
		UIUtil.addComponent(filterPanel, filterLabel, 10, 20, 120, 30);

		UIUtil.addComponent(filterPanel, filterField, 130, 20, 300, 30);

		JLabel filterExampleLabel = new JLabel("E.g. acc* / *.log");
		UIUtil.addComponent(filterPanel, filterExampleLabel, 440, 20, 120, 30);

		reportButtonPanel.setLayout(null);
		reportButtonPanel.setBorder(BorderFactory.createEtchedBorder());
		reportButtonPanel.setBackground(Color.LIGHT_GRAY);

		JButton genReportButton = new JButton("Generate Report");
		genReportButton.setActionCommand("GenerateReport");
		genReportButton.addActionListener(this);
		UIUtil.addComponent(reportButtonPanel, genReportButton, 330, 20, 120, 30);

		UIUtil.addComponent(contentPane, logFormatPanel, 2, 2, 892, 115);
		UIUtil.addComponent(contentPane, startTimePanel, 2, 118, 892, 85);
		UIUtil.addComponent(contentPane, endTimePanel, 2, 205, 892, 85);
		UIUtil.addComponent(contentPane, logFilesSelectPanel, 2, 290, 892, 65);
		UIUtil.addComponent(contentPane, percentilePanel, 2, 356, 892, 65);
		UIUtil.addComponent(contentPane, filterPanel, 2, 422, 892, 65);
		UIUtil.addComponent(contentPane, reportButtonPanel, 2, 488, 892, 65);

		setSize(900, 582);
		setVisible(true);
		setResizable(false);
		setReportStartTime();
		setReportEndTime();
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.revalidate();
		this.repaint();
	}

	/**
	 * This method is used to bind the action to the component.
	 * 
	 * @param e - ActionEvent performed.
	 * @return Nothing.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if(!basePanelAction(e) && !reportPanelAction(e)){
				graphPanelAction(e);
			}
		} catch (Exception exception) {
			LOGGER.error(exception);
		}
	}

	/**
	 * This method is used to bind the action to the component in Base Panel.
	 * 
	 * @param e - ActionEvent performed.
	 * @return boolean
	 * @throws AccessLogException
	 */
	private boolean basePanelAction(ActionEvent e) throws AccessLogException{
		boolean isBasePanelAction = false;
		if (Constants.CLEAR.equals(e.getActionCommand())) {
			logFormatField.setText("");
			logFormat = "";
			isBasePanelAction = true;
		} else if (Constants.getStartDateTimeList().contains(e.getActionCommand())) {
			setReportStartTime();
			isBasePanelAction = true;
		} else if (Constants.getEndDateTimeList().contains(e.getActionCommand())) {
			setReportEndTime();
			isBasePanelAction = true;
		} else if ("SelectBaseLogDirectory".equals(e.getActionCommand())) {
			UIUtil.selectLogBaseDirectory(logFilesDirectoryField);
			isBasePanelAction = true;
		} else if ("GenerateReport".equals(e.getActionCommand())) {
			validateAndGenerateReport();
			isBasePanelAction = true;
		} else if ("OHSRadio".equals(e.getActionCommand()) && ohsRadioButton.isSelected()){
			logFormatField.setText(Constants.OHS_DEFAULT_LOG_FORMAT);
			logFormat = logFormatField.getText().trim();
			isBasePanelAction = true;
			logFormatType = Constants.OHS_LOG_FORMAT_TYPE;
		} else if ("WeblogicRadio".equals(e.getActionCommand()) && weblogicRadioButton.isSelected()){
			logFormatField.setText("");
			logFormat = "";
			logFormatType = Constants.WEBLOGIC_LOG_FORMAT_TYPE;
			isBasePanelAction = true;
		}
		return isBasePanelAction;
	}
	
	/**
	 * This method is used to bind the action to the component in Report Panel.
	 * 
	 * @param e - ActionEvent performed.
	 * @return boolean
	 * @throws AccessLogException
	 */
	private boolean reportPanelAction(ActionEvent e) throws AccessLogException {
		boolean isReportPanelAction = false;
		try{
			if ("BackButton".equals(e.getActionCommand())) {
				reportTimePanel.setVisible(false);
				reportTablePanel.setVisible(false);
				reportBottomPanel.setVisible(false);

				logFormatPanel.setVisible(true);
				startTimePanel.setVisible(true);
				endTimePanel.setVisible(true);
				logFilesSelectPanel.setVisible(true);
				percentilePanel.setVisible(true);
				filterPanel.setVisible(true);
				reportButtonPanel.setVisible(true);

				contentPane.remove(reportTimePanel);
				contentPane.remove(reportTablePanel);
				contentPane.remove(reportBottomPanel);

				contentPane.revalidate();
				contentPane.repaint();
				isReportPanelAction = true;
				Thread.sleep(1000);
			} else if ("ExportExcel".equals(e.getActionCommand()) && report != null && !report.getReportBeanMap().isEmpty()) {
				UIUtil.exportReport(this, report, percentileCombo);
				isReportPanelAction = true;
			} else if ((Constants.GRAPH.equals(e.getActionCommand()) || "ResetUrlFilter".equals(e.getActionCommand())) && report != null && !report.getReportBeanMap().isEmpty() ) {
				openChart();
				isReportPanelAction = true;
			}
		} catch (Exception exception){
			throw new AccessLogException(exception);
		}
		return isReportPanelAction;
	}
	
	/**
	 * This method is used to bind the action to the component in Graph Panel.
	 * 
	 * @param e - ActionEvent performed.
	 * @return boolean
	 * @throws AccessLogException
	 */
	private void graphPanelAction(ActionEvent e) throws AccessLogException {
		if ("BackToReport".equals(e.getActionCommand())) {
			UIUtil.openReportPanel(this, contentPane);
		} else if ("AverageRadio".equals(e.getActionCommand()) || "PercentileRadio".equals(e.getActionCommand())
				|| "AscendingRadio".equals(e.getActionCommand()) || "DescendingRadio".equals(e.getActionCommand())){
			if((!averageRadioButton.isSelected() && !percentileRadioButton.isSelected()) || (!ascRadioButton.isSelected() && !descRadioButton.isSelected())){
				filterButton.setEnabled(false);
			} else {
				filterButton.setEnabled(true);
			}
		} else if (Constants.FILTER.equals(e.getActionCommand())){
			filterAndRedrawGraph();
		} else if ("UrlFilter".equals(e.getActionCommand())){
			List<String> selectedUrlList = urlMultiSelectList.getSelectedValuesList();
			if(selectedUrlList.isEmpty()){
				JOptionPane.showMessageDialog(this, "Select an item in list and try..", Constants.ERROR, JOptionPane.ERROR_MESSAGE);
			} else {
				drawGraphForSelectedUrls(selectedUrlList);
			}
		} 
	}
	
	/**
	 * Method used to draw graph for the urls passed in argument list.
	 * 
	 * @param selectedUrlList - List of urls selected in the list box.
	 */
	private void drawGraphForSelectedUrls(List<String> selectedUrlList) {
		while (datasetIndex >= 0) {
			this.plot.setDataset(this.datasetIndex, null);
			this.plot.setRenderer(this.datasetIndex, null);
			this.datasetIndex--;
		}
		Map<String, List<LogBean>> logBeanMap = report.getLogBeanMap();
		content.remove(legendScrollPane);
		DefaultListModel<String> listModel = (DefaultListModel<String>) urlMultiSelectList.getModel();
		listModel.removeAllElements();
		urlColorMap = new HashMap<>();
		for (Iterator<String> iterator = logBeanMap.keySet().iterator(); iterator.hasNext();) {
			String url = iterator.next();
			if(selectedUrlList.contains(url)){
				this.datasetIndex++;
				this.plot.setDataset(this.datasetIndex,UIUtil.createDataset(url, logBeanMap.get(url)));
				this.plot.setRenderer(this.datasetIndex, new StandardXYItemRenderer());
			}
		}
		legendPanel = UIUtil.createLegendPanel(this.plot, urlMultiSelectList, urlColorMap);
		legendScrollPane = new JScrollPane(legendPanel);
		legendScrollPane.setBorder(BorderFactory.createEtchedBorder());
		UIUtil.addComponent(content, legendScrollPane, 302, 402, 300, 150);
		content.repaint();
		content.revalidate();
		this.revalidate();
		this.repaint();
	}
	
	/**
	 *  Method to filter and redraw the graph based on Average/Percentile and Ascending/Descending.
	 */
	private void filterAndRedrawGraph() {
		boolean isAverageFilter = averageRadioButton.isSelected();
		boolean isAscendingFilter = ascRadioButton.isSelected();
		int topLimit = (Integer) topCombo.getSelectedItem();
		if (sortedUrlMapList == null) {
			return;
		}
		while (datasetIndex >= 0) {
			this.plot.setDataset(this.datasetIndex, null);
			this.plot.setRenderer(this.datasetIndex, null);
			this.datasetIndex--;
		}
		content.remove(legendScrollPane);
		DefaultListModel<String> listModel = (DefaultListModel<String>) urlMultiSelectList.getModel();
		listModel.removeAllElements();
		urlColorMap = new HashMap<>();
		List<ReportBean> averageUrlList = sortedUrlMapList.get("Average");
		List<ReportBean> percentileUrlList = sortedUrlMapList.get(Constants.PERCENTILE);
		if (isAverageFilter) {
			drawGraph(averageUrlList, isAscendingFilter, topLimit);
		} else {
			drawGraph(percentileUrlList, isAscendingFilter, topLimit);
		}
		legendPanel = UIUtil.createLegendPanel(this.plot, urlMultiSelectList, urlColorMap);
		legendScrollPane = new JScrollPane(legendPanel);
		legendScrollPane.setBorder(BorderFactory.createEtchedBorder());
		UIUtil.addComponent(content, legendScrollPane, 302, 402, 300, 150);
		content.repaint();
		content.revalidate();
		this.revalidate();
		this.repaint();
	}
	
	/**
	 * Method used to draw the graph and render in plot.
	 * 
	 * @param urlList - List of urls for which graph has to be drawn.
	 * @param isAscendingFilter - Ascending order or Descending Order.
	 * @param topLimit - Limit for which the graph has to be drawn.
	 */
	private void drawGraph(List<ReportBean> urlList, boolean isAscendingFilter, int topLimit){
		Map<String, List<LogBean>> logBeanMap = report.getLogBeanMap();
		if (isAscendingFilter) {
			for (int iterIndex = 0; iterIndex < topLimit; iterIndex++) {
				ReportBean reportBean = urlList.get(iterIndex);
				this.datasetIndex++;
				this.plot.setDataset(this.datasetIndex,	UIUtil.createDataset(reportBean.getUrl(), logBeanMap.get(reportBean.getUrl())));
				this.plot.setRenderer(this.datasetIndex, new StandardXYItemRenderer());
			}
		} else {
			for (int iterIndex = urlList.size()-1; iterIndex >= (urlList.size()- topLimit); iterIndex--) {
				ReportBean reportBean = urlList.get(iterIndex);
				this.datasetIndex++;
				this.plot.setDataset(this.datasetIndex, UIUtil.createDataset(reportBean.getUrl(), logBeanMap.get(reportBean.getUrl())));
				this.plot.setRenderer(this.datasetIndex, new StandardXYItemRenderer());
			}
		}
	}
	
	/**
	 * Class to render the color for each entry in List box.
	 * 
	 * @author  274664 (Ravikumar J), 323596 (Sankar S)
	 * @version 1.0
	 * @since   2017-5-26  
	 */
	class UrlListRenderer extends DefaultListCellRenderer {
		
		private static final long serialVersionUID = -749171698859122006L;

		@SuppressWarnings("rawtypes")
		@Override
	    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus ) {
	        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus );
	        setForeground(urlColorMap.get(value));
	        return this;
	    }
	}
	
	/**
	 * Method used to create the new chart panel
	 * 
	 * @throws AccessLogException
	 */
	private void openChart() throws AccessLogException {
		try {
			urlColorMap = new HashMap<>();
			DefaultListModel<String> listModel = (DefaultListModel<String>) urlMultiSelectList.getModel();
			listModel.removeAllElements();
			content.removeAll();
			content.setLayout(null);
		    content.setBorder(BorderFactory.createEtchedBorder());
			boolean firstDataset = true;
			Map<String, List<LogBean>> logBeanMap = report.getLogBeanMap();
			for (Iterator<String> iterator = logBeanMap.keySet().iterator(); iterator.hasNext();) {
				String url = iterator.next();
				if(firstDataset){
					this.datasetIndex++;
					TimeSeriesCollection dataset = UIUtil.createDataset(url,logBeanMap.get(url));
					chart = ChartFactory.createTimeSeriesChart(null, "Time", "Time Taken", dataset, true, true, false);
					chart.setBackgroundPaint(Color.white);
			        this.plot = chart.getXYPlot();
			        this.plot.setBackgroundPaint(Color.lightGray);
			        this.plot.setDomainGridlinePaint(Color.white);
			        this.plot.setRangeGridlinePaint(Color.white);
			        ValueAxis axis = this.plot.getDomainAxis();
			        axis.setAutoRange(true);
			        NumberAxis rangeAxis2 = new NumberAxis("Range Axis 2");
			        rangeAxis2.setAutoRangeIncludesZero(false);
			        ChartPanel chartPanel = new ChartPanel(chart);
			        chartPanel.setPreferredSize(new java.awt.Dimension(880, 390));
			        JScrollPane chartScrollPane = new JScrollPane(chartPanel);
			        chartScrollPane.setBorder(BorderFactory.createEtchedBorder());
			        UIUtil.addComponent(content, chartScrollPane, 2, 2, 890, 400);
			        firstDataset = false;
				} else {
					 this.datasetIndex++;
		             this.plot.setDataset(this.datasetIndex, UIUtil.createDataset(url,logBeanMap.get(url)));
		             this.plot.setRenderer(this.datasetIndex, new StandardXYItemRenderer());
				}
			}

			JPanel buttonPanel = new JPanel();
			buttonPanel.setLayout(null);
			buttonPanel.setBorder(BorderFactory.createEtchedBorder());
			
			filterButton.setActionCommand(Constants.FILTER);
			filterButton.addActionListener(this);
			filterButton.setEnabled(false);
			UIUtil.addComponent(buttonPanel, filterButton, 40, 100, 80, 30);
			
			JButton backButton = new JButton("Back");
			backButton.setActionCommand("BackToReport");
			backButton.addActionListener(this);
			UIUtil.addComponent(buttonPanel, backButton, 140, 100, 80, 30);
			
			JLabel sortLabel = new JLabel("Sort By :: ");
			averageRadioButton.setActionCommand("AverageRadio");
			averageRadioButton.setSelected(false);
			averageRadioButton.addActionListener(this);
			percentileRadioButton.setActionCommand("PercentileRadio");
			percentileRadioButton.setSelected(false);
			percentileRadioButton.addActionListener(this);
			ButtonGroup sortButtonGroup = new ButtonGroup();
			sortButtonGroup.add(averageRadioButton);
			sortButtonGroup.add(percentileRadioButton);
			sortButtonGroup.clearSelection();
			UIUtil.addComponent(buttonPanel, sortLabel, 10, 7, 60, 25);
			UIUtil.addComponent(buttonPanel, averageRadioButton, 74, 7, 100, 25);
			UIUtil.addComponent(buttonPanel, percentileRadioButton, 176, 7, 100, 25);
			
			JLabel orderLabel = new JLabel("Order By :: ");
			ascRadioButton.setActionCommand("AscendingRadio");
			ascRadioButton.setSelected(false);
			ascRadioButton.addActionListener(this);
			descRadioButton.setActionCommand("DescendingRadio");
			descRadioButton.setSelected(false);
			descRadioButton.addActionListener(this);
			ButtonGroup orderButtonGroup = new ButtonGroup();
			orderButtonGroup.add(ascRadioButton);
			orderButtonGroup.add(descRadioButton);
			orderButtonGroup.clearSelection();
			UIUtil.addComponent(buttonPanel, orderLabel, 10, 36, 60, 25);
			UIUtil.addComponent(buttonPanel, ascRadioButton, 74, 36, 100, 25);
			UIUtil.addComponent(buttonPanel, descRadioButton, 176, 36, 100, 25);
			
			JLabel topLabel = new JLabel("Top ");
			UIUtil.addComponent(buttonPanel, topLabel, 10, 63, 60, 25);
			
			for (int index = 1; index <=  logBeanMap.keySet().size(); index++) {
				topCombo.addItem(index);
			}
			topCombo.setSelectedIndex(0);
			UIUtil.addComponent(buttonPanel, topCombo, 74, 63, 100, 25);
			
			JLabel itemLabel = new JLabel("Items");
			UIUtil.addComponent(buttonPanel, itemLabel, 180, 63, 60, 25);
			
			UIUtil.addComponent(content, buttonPanel, 2, 402, 300, 150);
			
			legendPanel = UIUtil.createLegendPanel(this.plot, urlMultiSelectList, urlColorMap);
			legendScrollPane = new JScrollPane(legendPanel);
			legendScrollPane.setBorder(BorderFactory.createEtchedBorder());
			UIUtil.addComponent(content, legendScrollPane, 302, 402, 300, 150);

			JPanel urlBasedFilterPanel = new JPanel();
			urlBasedFilterPanel.setLayout(null);
			urlBasedFilterPanel.setBorder(BorderFactory.createEtchedBorder());
			
			urlMultiSelectList.setCellRenderer(new UrlListRenderer());
			JScrollPane urlFilterScrollPane = new JScrollPane(urlMultiSelectList);
			urlFilterScrollPane.setBorder(BorderFactory.createEtchedBorder());
			UIUtil.addComponent(urlBasedFilterPanel, urlFilterScrollPane, 2, 2, 286, 110);
			
			JButton urlFilterButton = new JButton("Extract");
			urlFilterButton.setActionCommand("UrlFilter");
			urlFilterButton.addActionListener(this);
			UIUtil.addComponent(urlBasedFilterPanel, urlFilterButton, 59, 114, 80, 30);
			
			JButton resetUrlFilterButton = new JButton("Reset");
			resetUrlFilterButton.setActionCommand("ResetUrlFilter");
			resetUrlFilterButton.addActionListener(this);
			UIUtil.addComponent(urlBasedFilterPanel, resetUrlFilterButton, 145, 114, 80, 30);
			
			UIUtil.addComponent(content, urlBasedFilterPanel, 602, 402, 290, 150);
			
			setContentPane(content);
			chart.removeLegend();
			this.revalidate();
			this.repaint();
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
	}

	/**
	 * This method is used to set the Report Start Time.
	 * 
	 * @return Nothing.
	 */
	public void setReportStartTime() {
		String reportStartTime = UIUtil.setReportTime(startYearCombo, startMonthCombo, startDayCombo, startHourCombo, startMinutesCombo, startSecondsCombo);
		startTimeValueLabel.setText(reportStartTime);
		reportStartDateTimeValue = reportStartTime;
	}

	/**
	 * This method is used to set the Report End Time.
	 * 
	 * @return Nothing.
	 */
	public void setReportEndTime() {
		String reportEndTime = UIUtil.setReportTime(endYearCombo, endMonthCombo, endDayCombo, endHourCombo, endMinutesCombo, endSecondsCombo);
		endTimeValueLabel.setText(reportEndTime);
		reportEndDateTimeValue = reportEndTime;
	}

	/**
	 * This method is used to validate and generate the report.
	 * 
	 * @return Nothing.
	 */
	public void validateAndGenerateReport() throws AccessLogException {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date startTime = simpleDateFormat.parse(reportStartDateTimeValue);
			Date endTime = simpleDateFormat.parse(reportEndDateTimeValue);
			if (logFormat.trim().length() == 0) {
				JOptionPane.showMessageDialog(this, "Log Format is Mandatory", Constants.ERROR, JOptionPane.ERROR_MESSAGE);
				return;
			} else if ((weblogicRadioButton.isSelected() && !validateWeblogicRelatedParameters()) || (ohsRadioButton.isSelected() && !validateOhsRelatedParameters())) {
				return;
			} else if (startTime.after(endTime)) {
				JOptionPane.showMessageDialog(this, "Start Time should be less than End Time", Constants.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			} else if (logFilesDirectoryField.getText().trim().length() == 0) {
				JOptionPane.showMessageDialog(this, "Select Log Base Directory", Constants.ERROR, JOptionPane.ERROR_MESSAGE);
				return;
			} else if (((Integer) percentileCombo.getSelectedItem()) < 50) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog(this, "Want to proceed with Percentile less than 50?",
						Constants.PERCENTILE, dialogButton);
				if (dialogResult == JOptionPane.NO_OPTION) {
					return;
				}
			}

			String fileNameFilter = filterField.getText().trim();
			if (fileNameFilter.length() == 0) {
				fileNameFilter = "*";
			}
			int percentile = (Integer) percentileCombo.getSelectedItem();
			report = new GenerateReport(logFormatType).getGeneratedReport(new File(logFilesDirectoryField.getText()),
					fileNameFilter, reportStartDateTimeValue, reportEndDateTimeValue, logFormat.trim(), percentile);
			sortedUrlMapList = ReportUtil.getSortedUrlList(report.getReportBeanMap());
			if (!buildReportPanel(report)) {
				JOptionPane.showMessageDialog(this, "Issue in Parsing Report. Contact System Administrator", Constants.ERROR,
						JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
	}
	
	/**
	 * Method to validate mandatory fields for weblogic logs.
	 * 
	 * @return boolean - true if success, else false.
	 */
	private boolean validateWeblogicRelatedParameters(){
		if (logFormat.trim().indexOf("date") < 0) {
			JOptionPane.showMessageDialog(this, "Date is Mandatory in Log Format", Constants.ERROR,
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (logFormat.trim().indexOf(" time") < 0 && logFormat.trim().indexOf("time ") < 0) {
			JOptionPane.showMessageDialog(this, "Time is Mandatory in Log Format", Constants.ERROR,
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (logFormat.trim().indexOf("time-taken") < 0) {
			JOptionPane.showMessageDialog(this, "Time Taken is Mandatory in Log Format", Constants.ERROR,
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (logFormat.trim().indexOf("cs-uri-stem") < 0 && logFormat.trim().indexOf("cs-uri") < 0) {
			JOptionPane.showMessageDialog(this, "URI Stem is Mandatory in Log Format", Constants.ERROR,
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	/**
	 * Method to validate mandatory fields for OHS logs.
	 * 
	 * @return boolean - true if success, else false.
	 */
	private boolean validateOhsRelatedParameters(){
		if (logFormat.trim().indexOf("%t") < 0) {
			JOptionPane.showMessageDialog(this, "Date Time is Mandatory in Log Format", Constants.ERROR,
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (logFormat.trim().indexOf("%D") < 0) {
			JOptionPane.showMessageDialog(this, "Time Taken is Mandatory in Log Format", Constants.ERROR,
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (logFormat.trim().indexOf("%U") < 0) {
			JOptionPane.showMessageDialog(this, "URI Stem is Mandatory in Log Format", Constants.ERROR,
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	/**
	 * This method is used to build the Report table.
	 * @param reportObjectList - List contains the report data
	 * 
	 * @return boolean.
	 */
	public boolean buildReportPanel(Report resultReport) throws AccessLogException {
		try {
			Map<String, ReportBean> reportBeanMap = resultReport.getReportBeanMap();

			if (reportBeanMap.size() <= 0 || reportBeanMap.keySet().isEmpty()) {
				return false;
			}

			Object[] column = { "S.No", "URL","No of Hits", "Average Resp Time",
					percentileCombo.getSelectedItem().toString() + "th Percentile" };
			int rowCount = reportBeanMap.keySet().size();

			Object[][] data = new Object[rowCount][column.length];
			DecimalFormat decimalFormat = new DecimalFormat("#.00");

			int rowIndex = 0;
			for (Iterator<String> iterator = reportBeanMap.keySet().iterator(); iterator.hasNext();) {
				String reportBeanMapKey = iterator.next();
				ReportBean reportBean = reportBeanMap.get(reportBeanMapKey);
				data[rowIndex][0] = Integer.valueOf(rowIndex + 1);
				data[rowIndex][1] = reportBeanMapKey;
				data[rowIndex][2] = reportBean.getNoOfHits();
				data[rowIndex][3] = Double.parseDouble(decimalFormat.format(reportBean.getAverage()));
				data[rowIndex][4] = Double.parseDouble(decimalFormat.format(reportBean.getPercentileValue()));
				rowIndex += 1;
			}

			TableModel tableModel = new DefaultTableModel(data, column) {
				private static final long serialVersionUID = -9141805951062647403L;

				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
				
				@Override
				public Class<?> getColumnClass(int columnIndex) {
					switch (columnIndex) {
                    case 0:
                    case 2:
                    	return Integer.class;
                    case 3:
                    case 4:
                        return Double.class;
                    default:
                        return String.class;
					}
				}
			};

			JTable reportTable = new JTable(tableModel);
			
			JScrollPane reportScrollPane = new JScrollPane(reportTable);

			DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
			rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);

			DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
			leftRenderer.setHorizontalAlignment(SwingConstants.LEFT);

			DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
			centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);

			TableColumn tableColumn0 = reportTable.getColumnModel().getColumn(0);
			tableColumn0.setHeaderRenderer(new ColumnRenderer(Color.GRAY, Color.WHITE, SwingConstants.CENTER));

			TableColumn tableColumn1 = reportTable.getColumnModel().getColumn(1);
			tableColumn1.setHeaderRenderer(new ColumnRenderer(Color.GRAY, Color.WHITE, SwingConstants.CENTER));

			TableColumn tableColumn2 = reportTable.getColumnModel().getColumn(2);
			tableColumn2.setHeaderRenderer(new ColumnRenderer(Color.GRAY, Color.WHITE, SwingConstants.CENTER));

			TableColumn tableColumn3 = reportTable.getColumnModel().getColumn(3);
			tableColumn3.setHeaderRenderer(new ColumnRenderer(Color.GRAY, Color.WHITE, SwingConstants.CENTER));
			
			TableColumn tableColumn4 = reportTable.getColumnModel().getColumn(4);
			tableColumn4.setHeaderRenderer(new ColumnRenderer(Color.GRAY, Color.WHITE, SwingConstants.CENTER));

			reportTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
			reportTable.getColumnModel().getColumn(0).setMaxWidth(100);
			reportTable.getColumnModel().getColumn(1).setCellRenderer(leftRenderer);
			reportTable.getColumnModel().getColumn(1).setMaxWidth(1000);
			reportTable.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
			reportTable.getColumnModel().getColumn(2).setMaxWidth(150);
			reportTable.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
			reportTable.getColumnModel().getColumn(3).setMaxWidth(250);
			reportTable.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
			reportTable.getColumnModel().getColumn(4).setMaxWidth(250);

			reportTable.setShowGrid(true);
			reportTable.setAutoCreateRowSorter(true);

			reportTimePanel = new JPanel();
			reportTablePanel = new JPanel();
			reportBottomPanel = new JPanel();

			reportTimePanel.setBorder(BorderFactory.createEtchedBorder());
			reportTimePanel.setLayout(null);
			reportTimePanel.setBackground(Color.LIGHT_GRAY);

			JLabel reportStartTimeLabel = new JLabel("\tStart Time :: " + reportStartDateTimeValue);
			reportStartTimeLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
			JLabel reportEndTimeLabel = new JLabel("\tEnd Time   :: " + reportEndDateTimeValue);
			reportEndTimeLabel.setFont(new Font("SansSerif", Font.BOLD, 12));

			UIUtil.addComponent(reportTimePanel, reportStartTimeLabel, 10, 10, 200, 25);
			UIUtil.addComponent(reportTimePanel, reportEndTimeLabel, 10, 40, 200, 25);

			reportTablePanel.setLayout(null);
			reportTablePanel.setBorder(BorderFactory.createLoweredBevelBorder());
			reportTablePanel.setBackground(Color.DARK_GRAY);

			UIUtil.addComponent(reportTablePanel, reportScrollPane, 2, 2, 887, 402);

			reportBottomPanel.setBorder(BorderFactory.createEtchedBorder());
			reportBottomPanel.setLayout(null);
			reportBottomPanel.setBackground(Color.LIGHT_GRAY);

			JButton exportExcelButton = new JButton("Export");
			exportExcelButton.setActionCommand("ExportExcel");
			exportExcelButton.addActionListener(this);
			JButton backButton = new JButton("Back");
			backButton.setActionCommand("BackButton");
			backButton.addActionListener(this);
			JButton graphButton = new JButton(Constants.GRAPH);
			graphButton.setActionCommand(Constants.GRAPH);
			graphButton.addActionListener(this);
			
			UIUtil.addComponent(reportBottomPanel, exportExcelButton, 300, 18, 70, 30);
			UIUtil.addComponent(reportBottomPanel, backButton, 390, 18, 70, 30);
			UIUtil.addComponent(reportBottomPanel, graphButton, 480, 18, 70, 30);

			logFormatPanel.setVisible(false);
			startTimePanel.setVisible(false);
			endTimePanel.setVisible(false);
			logFilesSelectPanel.setVisible(false);
			percentilePanel.setVisible(false);
			filterPanel.setVisible(false);
			reportButtonPanel.setVisible(false);

			UIUtil.addComponent(contentPane, reportTimePanel, 2, 2, 892, 75);
			UIUtil.addComponent(contentPane, reportTablePanel, 2, 78, 892, 409);
			UIUtil.addComponent(contentPane, reportBottomPanel, 2, 488, 892, 65);

			this.revalidate();
			this.repaint();

		} catch (Exception e) {
			throw new AccessLogException(e);
		}
		return true;
	}
}
