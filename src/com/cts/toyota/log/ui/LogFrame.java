package com.cts.toyota.log.ui;

import java.awt.Color;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;

import com.cts.toyota.log.bean.Report;
import com.cts.toyota.log.bean.ReportBean;
import com.cts.toyota.log.constants.Constants;

/**
 * Main Container Frame with all variables.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class LogFrame extends JFrame{

	private static final long serialVersionUID = 1L;

	protected JLabel logTypeLabel = new JLabel("Log Type :: ");
	
	protected JLabel logFormatLabel = new JLabel("Log Format :: ");
	
	protected JLabel startTimeLabel = new JLabel("Report Start Time :: ");
	
	protected JLabel startYearLabel = new JLabel("Year");
	
	protected JLabel startMonthLabel = new JLabel("Month");
	
	protected JLabel startDayLabel = new JLabel("Day");
	
	protected JLabel startHourLabel = new JLabel("Hour");
	
	protected JLabel startMinuteLabel = new JLabel("Minute");
	
	protected JLabel startSecondLabel = new JLabel("Second");
	
	protected JLabel endTimeLabel = new JLabel("Report End Time :: ");
	
	protected JLabel endYearLabel = new JLabel("Year");
	
	protected JLabel endMonthLabel = new JLabel("Month");
	
	protected JLabel endDayLabel = new JLabel("Day");
	
	protected JLabel endHourLabel = new JLabel("Hour");
	
	protected JLabel endMinuteLabel = new JLabel("Minute");
	
	protected JLabel endSecondLabel = new JLabel("Second");
	
	protected String logFormat = "";

	protected String reportStartDateTimeValue = "";

	protected String reportEndDateTimeValue = "";

	protected JTextField logFormatField = new JTextField();

	protected Date date = new Date();
	
	protected Calendar calendar = GregorianCalendar.getInstance();
	
	protected int year = calendar.get(Calendar.YEAR);

	protected JComboBox<Integer> startYearCombo = new JComboBox<>();

	protected JComboBox<Integer> startMonthCombo = new JComboBox<>();

	protected JComboBox<Integer> startDayCombo = new JComboBox<>();

	protected JComboBox<Integer> startHourCombo = new JComboBox<>();

	protected JComboBox<Integer> startMinutesCombo = new JComboBox<>();

	protected JComboBox<Integer> startSecondsCombo = new JComboBox<>();

	protected JComboBox<Integer> endYearCombo = new JComboBox<>();

	protected JComboBox<Integer> endMonthCombo = new JComboBox<>();

	protected JComboBox<Integer> endDayCombo = new JComboBox<>();

	protected JComboBox<Integer> endHourCombo = new JComboBox<>();

	protected JComboBox<Integer> endMinutesCombo = new JComboBox<>();

	protected JComboBox<Integer> endSecondsCombo = new JComboBox<>();

	protected JLabel startTimeValueLabel = new JLabel("");
	
	protected JLabel endTimeValueLabel = new JLabel("");

	protected JTextField logFilesDirectoryField = new JTextField();

	protected JComboBox<Integer> percentileCombo = new JComboBox<>();

	protected JTextField filterField = new JTextField();

	protected JPanel contentPane;
	
	protected JPanel logFormatPanel = new JPanel();
	
	protected JPanel startTimePanel = new JPanel();
	
	protected JPanel endTimePanel = new JPanel();
	
	protected JPanel logFilesSelectPanel = new JPanel();
	
	protected JPanel percentilePanel = new JPanel();
	
	protected JPanel filterPanel = new JPanel();
	
	protected JPanel reportButtonPanel = new JPanel();

	protected JPanel reportTimePanel;
	
	protected JPanel reportTablePanel;
	
	protected JPanel reportBottomPanel;

	protected Report report;
	
	protected XYPlot plot;
	    
	protected int datasetIndex = 0;
	
	protected JFreeChart chart;
	
	protected JComboBox<Integer> topCombo = new JComboBox<>();
	
	protected DefaultListModel<String> model = new DefaultListModel<>();
	
	protected JList<String> urlMultiSelectList = new JList<>(model);
	
	protected transient Map<String, Color> urlColorMap = new HashMap<>();
	
	protected JRadioButton averageRadioButton = new JRadioButton("Average");
	
	protected JRadioButton percentileRadioButton = new JRadioButton(Constants.PERCENTILE);
	
	protected JRadioButton ascRadioButton = new JRadioButton("Ascending");
	
	protected JRadioButton descRadioButton = new JRadioButton("Descending");
	
	protected JButton filterButton = new JButton(Constants.FILTER);
	
	protected transient Map<String, List<ReportBean>> sortedUrlMapList;
	
	protected JPanel legendPanel = new JPanel();
	
	protected JScrollPane legendScrollPane;
	
	protected JPanel content = new JPanel();
	
	protected JRadioButton weblogicRadioButton = new JRadioButton("Weblogic");
	
	protected JRadioButton ohsRadioButton = new JRadioButton("OHS");
	
	protected int logFormatType = Constants.WEBLOGIC_LOG_FORMAT_TYPE;
	
}
