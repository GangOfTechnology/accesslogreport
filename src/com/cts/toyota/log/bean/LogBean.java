package com.cts.toyota.log.bean;

import java.util.Date;
import java.util.Objects;

/**
 * Bean Class contains the access log related variables.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class LogBean implements Comparable<LogBean> {

	private Date date;

	private double timeTaken;

	private String url;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(double timeTaken) {
		this.timeTaken = timeTaken;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public int compareTo(LogBean other) {
		return date.compareTo(other.getDate());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) 
			return false;
		if (this.getClass() != obj.getClass()) 
			return false;
		LogBean otherBean = (LogBean) obj;
		return (this.getDate().equals(otherBean.getDate()) && this.getUrl().equals(otherBean.getUrl()));
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, url);
	}
}
