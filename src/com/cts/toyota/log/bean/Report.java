package com.cts.toyota.log.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Bean Class contains the Report Bean Map and Access log Map.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class Report implements Serializable{

	private static final long serialVersionUID = -923731683195776001L;

	private transient Map<String, ReportBean> reportBeanMap;
	
	private transient Map<String, List<LogBean>> logBeanMap;

	public Map<String, ReportBean> getReportBeanMap() {
		return reportBeanMap;
	}

	public void setReportBeanMap(Map<String, ReportBean> reportBeanMap) {
		this.reportBeanMap = reportBeanMap;
	}

	public Map<String, List<LogBean>> getLogBeanMap() {
		return logBeanMap;
	}

	public void setLogBeanMap(Map<String, List<LogBean>> logBeanMap) {
		this.logBeanMap = logBeanMap;
	}
}
