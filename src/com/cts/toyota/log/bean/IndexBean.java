package com.cts.toyota.log.bean;

/**
 * Bean Class contains the indexes for all the log format.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class IndexBean {

	private Integer dateIndex;
	
	private Integer timeIndex;
	
	private Integer urlIndex;
	
	private Integer timeTakenIndex;

	public Integer getDateIndex() {
		return dateIndex;
	}

	public void setDateIndex(Integer dateIndex) {
		this.dateIndex = dateIndex;
	}

	public Integer getTimeIndex() {
		return timeIndex;
	}

	public void setTimeIndex(Integer timeIndex) {
		this.timeIndex = timeIndex;
	}

	public Integer getUrlIndex() {
		return urlIndex;
	}

	public void setUrlIndex(Integer urlIndex) {
		this.urlIndex = urlIndex;
	}
	
	public Integer getTimeTakenIndex() {
		return timeTakenIndex;
	}

	public void setTimeTakenIndex(Integer timeTakenIndex) {
		this.timeTakenIndex = timeTakenIndex;
	}
	
}
