package com.cts.toyota.log.bean;

import java.io.Serializable;
import java.util.Objects;

/**
 * Bean Class contains the average and percentile value.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class ReportBean implements Comparable<ReportBean>, Serializable{
	
	private static final long serialVersionUID = -3625198842967089023L;

	private String url;

	private Double average;

	private Double percentileValue;

	private String percentile; 
	
	private Integer noOfHits; 

	public Integer getNoOfHits() {
		return noOfHits;
	}

	public void setNoOfHits(Integer noOfHits) {
		this.noOfHits = noOfHits;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public Double getAverage() {
		return average;
	}

	public void setAverage(Double average) {
		this.average = average;
	}

	public Double getPercentileValue() {
		return percentileValue;
	}

	public void setPercentileValue(Double percentileValue) {
		this.percentileValue = percentileValue;
	}

	public String getPercentile() {
		return percentile;
	}

	public void setPercentile(String percentile) {
		this.percentile = percentile;
	}
	
	@Override
	public int compareTo(ReportBean other) {
		if(other.getAverage().doubleValue() > this.getAverage().doubleValue()) {
			return -1;
		} else if (other.getAverage().doubleValue() < this.getAverage().doubleValue()){
			return 1;
		}
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) 
			return false;
		if (this.getClass() != obj.getClass()) 
			return false;
		ReportBean otherBean = (ReportBean) obj;
		return (bitwiseEqualsWithCanonicalNaN(this.getAverage().doubleValue(), otherBean.getAverage().doubleValue()) && 
			bitwiseEqualsWithCanonicalNaN(this.getPercentileValue().doubleValue(), otherBean.getPercentileValue().doubleValue()) && 
			this.getPercentile().equals(otherBean.getPercentile()) && this.getUrl().equals(otherBean.getUrl()));
	}

	@Override
	public int hashCode() {
		return Objects.hash(url, percentile, percentileValue, average);
	}
	
	public int compareOnPercentile(ReportBean other) {
		if(other.getPercentileValue() > this.getPercentileValue()) {
			return 1;
		} else if (other.getPercentileValue() < this.getPercentileValue()){
			return -1;
		}
		return 0;
	}
	
	private boolean bitwiseEqualsWithCanonicalNaN(double x, double y) {
	    return Double.doubleToLongBits(x) == Double.doubleToLongBits(y);
	}
	
	public ReportBean getAvgExchPercBean(ReportBean bean){
		ReportBean reportBean = new ReportBean();
		reportBean.setUrl(bean.getUrl());
		reportBean.setPercentile(bean.getPercentile());
		reportBean.setAverage(bean.getPercentileValue());
		reportBean.setPercentileValue(bean.getAverage());
		return reportBean;
	}
}
