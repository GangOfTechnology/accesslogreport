package com.cts.toyota.log.excel;

import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cts.toyota.log.bean.ReportBean;
import com.cts.toyota.log.exception.AccessLogException;

/**
 * Class to export the report as MS Excel using Apache POI.
 * 
 * @author  274664 (Ravikumar J), 323596 (Sankar S)
 * @version 1.0
 * @since   2017-5-26  
 */
public class ExportToExcel {

	Workbook workBook = new XSSFWorkbook();
	
	private static DecimalFormat decimalFormat = new DecimalFormat("#.00");

	/**
	 * Method to export the report in Excel.
	 * 
	 * @param reportMap - Map contains the url and its average and percentile value.
	 * @param outputFileName - Filename in which the report has to be generated.
	 * @param percentile - Percentile for which the report has to be generated.
	 * @return boolean - Returns true if report exported successfully, else return false.
	 * @throws AccessLogException
	 */
	public boolean exportDateToExcel(Map<String, ReportBean> reportMap, String outputFileName, int percentile) throws AccessLogException {
		try {
			int rowNum = 0;
			Sheet sheet = workBook.createSheet("Report");
			Row row;
			Cell cell;
			row = sheet.createRow(rowNum);
			CellStyle headerStyle = getCellStyle(0, 0);

			cell = row.createCell((short) 0);
			cell.setCellStyle(headerStyle);
			cell.setCellValue("S.No");
			cell = row.createCell((short) 1);
			cell.setCellStyle(headerStyle);
			cell.setCellValue("URL");
			cell = row.createCell((short) 2);
			cell.setCellStyle(headerStyle);
			cell.setCellValue("No of Hits");
			cell = row.createCell((short) 3);
			cell.setCellStyle(headerStyle);
			cell.setCellValue("Average Resp Time");
			cell = row.createCell((short) 4);
			cell.setCellStyle(headerStyle);
			cell.setCellValue(percentile + "th Percentile");
			rowNum += 1;

			for (Iterator<String> iterator = reportMap.keySet().iterator(); iterator.hasNext();) {
				String reportMapKey = iterator.next();
				ReportBean reportBean = reportMap.get(reportMapKey);
				row = sheet.createRow(rowNum);
				cell = row.createCell((short) 0);
				cell.setCellStyle(getCellStyle(rowNum, 0));
				cell.setCellValue(rowNum);
				cell = row.createCell((short) 1);
				cell.setCellStyle(getCellStyle(rowNum, 1));
				cell.setCellValue(reportMapKey);
				cell = row.createCell((short) 2);
				cell.setCellStyle(getCellStyle(rowNum, 2));
				cell.setCellValue(reportBean.getNoOfHits());
				cell = row.createCell((short) 3);
				cell.setCellStyle(getCellStyle(rowNum, 3));
				cell.setCellValue(Double.parseDouble(decimalFormat.format(reportBean.getAverage())));
				cell = row.createCell((short) 4);
				cell.setCellStyle(getCellStyle(rowNum, 4));
				cell.setCellValue(Double.parseDouble(decimalFormat.format(reportBean.getPercentileValue())));
				rowNum += 1;
			}

			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
			sheet.autoSizeColumn(3);
			sheet.autoSizeColumn(4);

			FileOutputStream out = new FileOutputStream(outputFileName);
			workBook.write(out);
			out.close();
		} catch (Exception e) {
			throw new AccessLogException(e);
		}
		return true;
	}

	/**
	 * Method to set the cell style.
	 * 
	 * @param row - Row No of the sheet in workbook.
	 * @param column - Column No of the sheet in workbook.
	 * @return CellStyle - Return the Cell Style.
	 */
	public CellStyle getCellStyle(int row, int column) {
		CellStyle style = workBook.createCellStyle();

		if (row == 0) {
			style.setFillForegroundColor(IndexedColors.GREY_80_PERCENT.getIndex());
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			Font font = workBook.createFont();
			font.setColor(IndexedColors.WHITE.getIndex());
			font.setBold(true);
			style.setFont(font);
			style.setBorderBottom(BorderStyle.MEDIUM);
			style.setBorderTop(BorderStyle.MEDIUM);
			style.setBorderRight(BorderStyle.MEDIUM);
			style.setBorderLeft(BorderStyle.MEDIUM);
			style.setAlignment(HorizontalAlignment.CENTER);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
		} else if (row % 2 == 1) {
			style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			Font font = workBook.createFont();
			font.setColor(IndexedColors.BLACK.getIndex());
			style.setFont(font);
			style.setBorderBottom(BorderStyle.THIN);
			style.setBorderTop(BorderStyle.THIN);
			style.setBorderRight(BorderStyle.THIN);
			style.setBorderLeft(BorderStyle.THIN);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
			if (column == 0) {
				style.setAlignment(HorizontalAlignment.CENTER);
			} else if (column == 1) {
				style.setAlignment(HorizontalAlignment.LEFT);
			} else {
				style.setAlignment(HorizontalAlignment.RIGHT);
			}
		} else {
			style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			Font font = workBook.createFont();
			font.setColor(IndexedColors.BLACK.getIndex());
			style.setFont(font);
			style.setBorderBottom(BorderStyle.THIN);
			style.setBorderTop(BorderStyle.THIN);
			style.setBorderRight(BorderStyle.THIN);
			style.setBorderLeft(BorderStyle.THIN);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
			if (column == 0) {
				style.setAlignment(HorizontalAlignment.CENTER);
			} else if (column == 1) {
				style.setAlignment(HorizontalAlignment.LEFT);
			} else {
				style.setAlignment(HorizontalAlignment.RIGHT);
			}
		}
		return style;
	}
}
